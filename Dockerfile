FROM debian:jessie-slim

MAINTAINER Fedor Marchenko <mfs90@mail.ru> (@fmarchenko)

ENV LANG en_US.utf8
ENV WORKDIR=/home/dude

RUN apt-get update && \
    apt-get install -y locales apt-transport-https wget gnupg2 && \
    rm -rf /var/lib/apt/lists/* && \
    localedef -i en_US -c -f UTF-8 -A /usr/share/locale/locale.alias en_US.UTF-8

# Создание пользователя
RUN groupadd -g 999 dudes && \
    useradd -u 999 -rm -g dudes -d ${WORKDIR} -s /bin/bash dude

RUN wget -qO - https://apt.z.cash/zcash.asc | apt-key add - && \
    echo "deb [arch=amd64] https://apt.z.cash/ jessie main" | tee /etc/apt/sources.list.d/zcash.list && \
    apt-get update && apt-get install -y zcash && \
    rm -rf /var/lib/apt/lists/*

VOLUME ${WORKDIR}/.zcash-params
VOLUME ${WORKDIR}/.zcash

USER dude
WORKDIR ${WORKDIR}

ENTRYPOINT ["/usr/bin/zcashd", "-printtoconsole"]
