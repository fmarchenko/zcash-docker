# ZCash Docker node

1. Running with Compose

```sh
docker-compose up
```

2. Running with pure Docker

    2.1. For first run you need fetch params

    ```sh
    docker run --rm -v /data/.zcash:/home/dude/.zcash -v /data/.zcash-params:/home/dude/.zcash-params --entrypoint zcash-fetch-params fmarchenko/zcash
    ```

    2.2. Run Docker as daemon

    ```sh
    docker run -d -v /data/.zcash:/home/dude/.zcash -v /data/.zcash-params:/home/dude/.zcash-params --name zcash fmarchenko/zcash
    ```

    2.3. Using CLI

    ```sh
    docker exec -it zcash zcash-cli
    ```